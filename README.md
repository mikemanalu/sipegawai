## How to run Project!
 
1. Database = phpmyadmin 
    * name database = pegawai
    * password = null
2. Web Server ( Xampp )

**Step by Step Run the Application**

1.**Run** composer install.
2. **Run** cp .env.example .env
3. Copy **file env.txt** to **.env**
4. **Run** php artisan key:generate
5. **Run** php artisan storage:link
6. **Run** php artisan migrate
7. **Run** php artisan db:seed
8. **Run** php artisan serve
9. Login as Admin.
10. Enter company information such as company name, address, city, then no.telp, etc.


**Account *Default* System**

| Account  | Email | Pass |
| ----- | ----- | ---------|
| Admin   | admin@gmail.com  | 123456|

